#!/bin/bash

export PATH="$(dirname $PWD):$PATH"

# The test name is the path and script name
tests=(
  "test-counts"
  "test-rra"
  "test-rra-pathway"
  "test-rra-flute"
  "test-mle"
  #"test-mle-pathway" -- Need to transition to fgsea
)

run_test() {
  echo "   $1..."
  log=$(cd $1 && ./${1}.sh 2>&1) 
  
  if [ "$?" != "0" ]; then
    echo ""
    echo "ERROR running test: $1"
    echo "---"
    echo "$(echo "$log" | tail)"
    echo "---"
    echo "See $1/output/$1.log for more information"
    exit 1
  fi
}

echo "Running tests:"
for i in ${tests[@]}; do
  run_test "$i"
done

