#!/bin/bash 
set -eo pipefail

resource_path="$(dirname $(which mageck-workflow))/resources"
target="output/test-rra-flute/mageck-rra/rapa-vs-early/MAGeCKFlute_rapa-vs-early"

# Remove targets
[ -e $target ] && rm -r $target
find output -type f -name "*.log" -delete

mageck-workflow \
  --config config.yaml \
  --output_prefix output \
  --targets $resource_path/libraries/Brunello_fix.csv \
  --continue \
  --verbose
