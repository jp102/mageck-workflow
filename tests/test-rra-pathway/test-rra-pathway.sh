#!/bin/bash 
set -eop pipefail

resource_path="$(dirname $(which mageck-workflow))/resources"

# Remove targets
find output -type f -name "*.pathway_summary.txt" -delete
find output -type f -name "*.log" -delete

mageck-workflow \
  --config config.yaml \
  --output_prefix output \
  --targets $resource_path/libraries/Brunello_fix.csv \
  --continue \
  --verbose
