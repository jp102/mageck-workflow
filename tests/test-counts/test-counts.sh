#!/bin/bash 
set -eo pipefail

[ -e "output" ] && rm -r "output"

resource_path="$(dirname $(which mageck-workflow))/resources"

mageck-workflow \
  --targets $resource_path/libraries/Brunello_fix.csv \
  --config config.yaml \
  --verbose
