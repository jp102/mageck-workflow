#!/bin/bash 
set -eo pipefail

target_path="output/test-rra/mageck-rra"
resource_path="$(dirname $(which mageck-workflow))/resources"

[ -d $target_path ] && rm -r $target_path 
[ -d output ] && find output -name "*.log" -delete

mageck-workflow \
  --config config.yaml \
  --output_prefix output \
  --targets $resource_path/libraries/Brunello_fix.csv \
  --continue \
  --verbose
