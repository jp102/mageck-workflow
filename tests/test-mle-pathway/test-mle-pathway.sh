#!/bin/bash 
set -eo pipefail

target_path="output/test-mle-pathway/mageck-mle-pathway"
resource_path="$(dirname $(which mageck-workflow))/resources"

[ -d $target_path ] && rm -r $target_path 
[ -d output ] && find output -name "*.log" -delete

echo "Skipping test..."
#mageck-workflow \
#  --config config.yaml \
#  --output_prefix output \
#  --targets $resource_path/libraries/Brunello_fix.csv \
#  --continue \
#  --verbose
