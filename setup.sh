#!/bin/bash
set -o pipefail

path="$PWD"
conda_linux_url="https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"
conda_darwin_url="https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh"

error() {
  echo "Error: $@"
  exit 1
}

help_message() {
  echo "Usage: ./setup.sh [OPITON]..."
  echo "Options:"
  echo "  -r, --run      run setup"
  echo "  -v, --verbose  display logs as setup runs"
  echo "  -h, --help     display this message"
  exit
}

# Command line options
log=/dev/null
run=""
while [[ "$#" -gt 0 ]]; do
  case $1 in
    -v|--verbose) log=/dev/stdout; shift ;;
    -r|--run) run=true ;;
    -h|--help) print_help ;;
    *) echo "Unknown parameter passed: $1" && help_message ;;
  esac
  shift
done

# Requre run
[ $run ] || help_message

# Existing conda directory
if [ -d "$path/conda" ]; then
  error "Conda directory already exists. To reinstall, remove and try again."
fi

# Determine URL
echo "Setting up MAGECK workflow..."
case $OSTYPE in 
  linux*)
    echo "---Downloading conda for Linux..."
    conda_url=$conda_linux_url
    ;;
  darwin*)
    echo "---Downloading conda for Mac..."
    conda_url=$conda_darwin_url
    ;;
  *)
    error "Could not determine OS type or unsupported OS \"$OSTYPE\""
esac
conda_file=$(basename $conda_url)
  
# Download
(
  [ -e $conda_file ] && rm $conda_file
  curl -O $conda_url
) &> $log || error "Could not download Miniconda from: $conda_url"

echo "---Installing conda..."
(
  bash $conda_file -b -p "$path/conda"
  mv $conda_file "$path/conda" 
) &> $log || error "Could not install conda"

echo "---Installing mamba..."
(
  source conda/bin/activate base
  conda install mamba -c conda-forge -y
  mamba update conda -y
) &> $log || error "Could not install mamba"

echo "---Installing analysis environment..."
(
  source conda/bin/activate base
  mamba env create -f envs/workflow.yaml 
) &> $log || error "Could not create analysis environment"

echo "All done, setup sucessful! To get started, run \"./mageck-workflow --help\""
