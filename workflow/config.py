import argparse
import logging
import os
import shutil
import sys
import time
import yaml
import glob

import pandas as pd
import itertools as it

from os import path
from workflow.version import __version__
from workflow.logging import logger, add_logfile, set_verbose

# Defaults
PROG = "mageck-workflow"
DESC = f"Duke Functional Genomics MAGeCK workflow ({__version__})"
REQUIRED = ["project", "samples", "targets"]

# Paths that will be interpreted as absolute
ABSPATH = [
    "samples",
    "targets",
    "mle_design",
    "cnv_norm",
    "control_sgrna",
    "control_genes",
    "output_prefix",
    "input_prefix",
]


def to_list(dat):
    if dat is None or isinstance(dat, list):
        ret = dat
    else:
        try:
            ret = [dat]
        except Exception as e:
            logger.error(f"Unable to cast '{dat}' to list")
            raise e
    return ret

def check_abspath(loc):
    if not path.exists(loc):
        logger.error(f"Could not find file in sample sheet: {loc}")
        raise FileNotFoundError
    else:
        return path.abspath(loc)

def load_config(args, parser):
    logger.debug("Loading configuration from file...")
    try:
        cfgfile = open(args["config"], "r")
        config = yaml.safe_load(cfgfile)
        cfgfile.close()
    except FileNotFoundError:
        parser.error(f"Could not find configuration file: {args['config']}")

    for key, val in config.items():
        if key not in args:
            parser.error(f"Unrecognized option in configuration file: \"{key}\".\nSee --help for available options")
        else:
            args[key] = val
    return args


def arg_parser(desc=DESC, prog=PROG):
    parser = argparse.ArgumentParser(prog=prog, description=desc)

    parser.add_argument(
        "--project",
        metavar="<string>",
        help="project name and location to output results",
    )

    parser.add_argument(
        "--mle_design",
        metavar="<design file>",
        help="design matrix for MLE analysis (off by default, required for MLE)",
    )

    parser.add_argument(
        "--samples",
        metavar="<sample file>",
        help="file describing samples and conditions",
    )

    parser.add_argument(
        "--targets",
        metavar="<target file>",
        help="file describing CRISPR targets and their sequences",
    )

    # Optional
    parser.add_argument(
        "--treatment",
        metavar="<condition>",
        nargs="*",
        help="treatment conditions (one or more) to use in RRA analysis",
    )

    parser.add_argument(
        "--control",
        metavar="<condition>",
        nargs="*",
        help="control conditions (one or more) to use for in RRA analysis",
    )

    parser.add_argument(
        "--species",
        default="human",
        metavar="<species>",
        help="species to use for gene-set enrichment analysis (default: human)",
    )

    parser.add_argument(
        "--cnv_norm",
        metavar="<cnv file>",
        help="copy number variation data across cell lines to normalize against",
    )

    parser.add_argument(
        "--control_sgrna",
        metavar="<sgrna file>",
        help="list of control sgRNAs to normalize against",
    )

    parser.add_argument(
        "--control_genes",
        metavar="<gene file>",
        help="list of control genes to normalize against",
    )

    parser.add_argument(
        "--cell_line",
        metavar="<cell-line>",
        help="name of the cell line to be used for copy number normalization",
    )

    parser.add_argument(
        "--no_rra",
        default=False,
        action="store_true",
        help="do not run MAGeCK RRA workflow (on by default, tests all vs. all)",
    )

    parser.add_argument(
        "--no_pathway",
        default=False,
        action="store_true",
        help="do not run gene-set enrichment workflow",
    )

    parser.add_argument(
        "--no_flute",
        default=False,
        action="store_true",
        help="do not run MAGeCKFlute on any comparisons"
    )
    parser.add_argument(
        "--no_counts",
        default=False,
        action="store_true",
        help="do not tabulate sgRNA counts"
    )

    # Defaults
    parser.add_argument(
        "--mismatch",
        default=False,
        action="store_true",
        help="allow a single mismatch between the read and sgRNA sequence",
    )

    parser.add_argument(
        "--constant_sequence",
        metavar="<sequence>",
        default="TCTTGTGGAAAGGACGAAACACCG",
        help="sgRNA constant sequence (default: 'TCTTGTGGAAAGGACGAAACACCG')",
    )

    parser.add_argument(
        "--input_prefix",
        metavar="<path>",
        default=os.getcwd(),
        help="location to look for samples (default is current directory)",
    )

    parser.add_argument(
        "--output_prefix",
        metavar="<path>",
        default=os.getcwd(),
        help="location to save results (default is current directory)",
    )

    parser.add_argument(
        "--force",
        action="store_true",
        help="remove existing project folder and existing data",
    )

    parser.add_argument(
        "--processes",
        metavar="<number>",
        default=os.cpu_count() // 2,
        help="number of concurrent tasks to run (default is half the number of CPUs)",
    )

    parser.add_argument(
        "--counter",
        metavar="<program>",
        default=os.environ["BARCODE_COUNTER"],
        help="path to Java based barcode-counter",
    )

    parser.add_argument(
        "--rra_options",
        metavar="<options>",
        help="additional options to pass to mageck-rra",
    )

    parser.add_argument(
        "--config", metavar="<config>", help="analysis configuration file"
    )

    parser.add_argument(
        "--continue", action="store_true", help="add to / continue existing analysis"
    )

    parser.add_argument("--verbose", action="store_true", help="more detailed logging")

    # Read args from cli and convert to dict
    args = vars(parser.parse_args())

    # Set logging level
    if args["verbose"]:
        set_verbose()

    # Load config and overwrite values in cli with those supplied
    if args["config"]:
        args = load_config(args, parser)

    # Add defaults for snakemake
    default_path = path.abspath(path.dirname(__file__))
    if "snakefile" not in args or args["snakemake"] is None:
        args.update({"snakefile": path.join(default_path, "Snakefile")})

    if "rules_path" not in args or args["rules_path"] is None:
        args.update({"rules_path": path.join(default_path, "rules")})

    if "work_path" not in args or args["work_path"] is None:
        args.update({"work_path": path.join(path.dirname(default_path))})

    if "env_path" not in args or args["env_path"] is None:
        args.update({"env_path": path.join(args["work_path"], "envs")})

    if "run_path" not in args or args["run_path"] is None:
        args.update({"run_path": path.abspath(os.getcwd())})

    if "resource_path" not in args or args["resource_path"] is None:
        args.update(
            {"resource_path": path.join(path.dirname(default_path), "resources")}
        )

    if "gsea_file" not in args or args["gsea_file"] is None:
        args.update({"gsea_file": path.join(args["resource_path"], "gsea.yaml")})

    # Ensure all required arguments were found
    for req in REQUIRED:
        if req not in args or args[req] is None:
            mesg = f"the following argument is required: '{req}'"
            parser.error(mesg)

    # Inconsistencies
    if args["treatment"] and not args["control"]:
        parser.error("'--control' required when specifying '--treatment'")
    if args["control"] and not args["treatment"]:
        parser.error("'--treatment' required when specifying '--control'")
    if args["force"] and args["continue"]:
        parser.error("'--force' and '--continue' are inconstant")

    # Use absolute paths
    args.update(
        {k: path.abspath(v) for k, v in args.items() if k in ABSPATH and v is not None}
    )

    # Variable types
    args["treatment"] = to_list(args["treatment"])
    args["control"] = to_list(args["control"])

    # Add derived values
    args["project_path"] = path.join(args["output_prefix"], args["project"])
    args["logfile"] = path.join(args["project_path"], args["project"] + ".log")
    args["configfile"] = path.join(args["project_path"], "config.yaml")
    args["samplefile"] = path.join(args["project_path"], "samples.csv")
    return args


def write_samplefile(args):
    # Returns number of samples found
    try:
        ret = pd.read_csv(args["samples"])
    except FileNotFoundError:
        logger.error("Could not find sample file, please check this and try again")
        raise FileNotFoundError

    # Ensure prefix / barcode is present
    if not any([True for v in ret.columns if v in ["barcode", "prefix_length"]]):
        logger.error(
            "Sample file must include either 'barcode' or 'prefix_length' column"
        )
        raise ValueError

    # Ensure 'condition' is present in sample file doing rra
    if not args["no_rra"] and "condition" not in ret.columns:
        logger.error("Sample file must include 'condition' when --method is 'rra'")
        raise ValueError

    # Apply input_prefix if available
    if args["input_prefix"]:
        ret.path = [path.join(args["input_prefix"], v) for v in ret.path]

    # Get absolute paths of input files
    ret.path = ret.path.apply(check_abspath)

    # Write
    logger.debug(f"Writing samplefile to: {args['samplefile']}")
    ret.to_csv(args["samplefile"], index=False)
    return ret


def get_analyses(args, samples):
    # return analyses a dict of:
    #   <analysis-name>: {"control": <condition>, "treatment": <condition>}
    vals = samples.condition.values
    treat = args["treatment"]
    contr = args["control"]
    if treat and contr:
        if not all([v in vals for v in treat + contr]):
            logger.error(f"Could not find value for treatment or control in  in sample file")
            raise ValueError
        analyses = [(t, c) for t in treat for c in contr]
    else:
        analyses = [(v[1], v[0]) for v in it.combinations(pd.unique(vals), 2)]
    ret = {f"{v[0]}-vs-{v[1]}": {"treatment": v[0], "control": v[1]} for v in analyses}
    logger.info("Preparing to run the following analyses via RRA:")
    logger.info(str(pd.DataFrame(ret).transpose()) + "\n")
    return ret


def check_gsea_config(args):
    try:
        with open(args["gsea_file"], "r") as gseafile:
            species = yaml.safe_load(gseafile)['species']
    except Exception as e:
        logger.debug(e)
        logger.error(f"Could not open GSEA configuration file: {args['gsea_file']}")
        raise ValueError

    try:
        if args['species'] not in species:
            logger.error(
                f"Could not find {args['species']} in GSEA configuration file. "
                "Species available: " + ". ".join(species)
            )
            raise ValueError
    except Exception as e:
        logger.debug(e)
        logger.error(
            f"Could not parse GSEA configuration file, make sure this is properly formatted"
        )
        raise e


def get_args():
    # Get arguments, and validate
    args = arg_parser()

    # Hello message
    logger.info(
        f"Preparing to process project: {args['project']}\n"
        + f"Output directory: {args['project_path']}"
    )

    # Ensure project path does not already exist
    if path.exists(args["project_path"]):
        if args["force"]:
            logger.warn("Existing project found, overwriting (ctrl-c to abort)...")
            try:
                time.sleep(4)
            except:
                logger.error("Aborting at user's request")
                sys.exit(1)
            shutil.rmtree(args["project_path"])
        elif args["continue"]:
            logger.warn(
                "Continuing with existing project, "
                "providence will not be maintained  (ctrl-c to abort)..."
            )
            try:
                time.sleep(4)
            except:
                logger.error("Aborting at user's request")
                sys.exit(1)
        else:
            logger.error("Existing project found, remove, use '--force' to overwrite")
            raise FileExistsError

    if not args["continue"]:
        try:
            os.makedirs(args["project_path"], exist_ok=False)
        except Exception as e:
            logging.error(e)
        add_logfile(args["logfile"], logger, append=False)
    else:
        add_logfile(args["logfile"], logger, append=True)

    # Get samples files
    samples = write_samplefile(args)

    # Report plan
    if args["mle_design"]:
        logger.debug(f"Preparing to conduct MLE analysis using: {args['mle_design']}")
    else:
        logger.debug("No MLE design file given, MLE analysis will not be conducted...")

    # Get analyses 
    if args["no_rra"]:
        logger.debug("RRA analysis will not be conducted...")
        if not args["no_flute"]:
           pass # What do? 
    else:
        args["analyses"] = get_analyses(args, samples)


    if args["no_pathway"]:
        logger.debug("Pathway analysis will not be conducted...")

    if args["no_flute"]:
        logger.debug("MAGeCKFlute will not be run...")


    # Check GSEA and species
    check_gsea_config(args)

    # Write config
    logger.debug(f"Writing configuration to: {args['configfile']}")
    with open(args["configfile"], "w") as confout:
        confout.writelines(yaml.dump(args, default_flow_style=False))

    return args
