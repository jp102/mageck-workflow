import logging
import time
import sys

VERBOSE_LOGGERS = ["snakemake.logging", "root"]

# Start logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Silence noisy loggers
logging.getLogger("filelock").setLevel(logging.ERROR)


class ConsolFormatter(logging.Formatter):
    default = "%(message)s"
    FORMATS = {
        logging.INFO: default,
        logging.DEBUG: default,
        logging.WARNING: "Warning: " + default,
        logging.ERROR: "\nERROR: " + default,
        logging.CRITICAL: "\nCRITICAL: " + default,
    }

    def format(self, record):
        if record.name.startswith("snakemake.") and record.levelname in (
            "INFO",
            "WARNING",
            "ERROR"
        ):
            fmt = self.default
        else:
            fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(fmt)
        return formatter.format(record)


class FileFormatter(logging.Formatter):
    default = "[%(asctime)s] %(levelname)s: %(message)s"
    snakemake = "%(message)s"

    def format(self, record):
        if record.name.startswith("snakemake."):
            record.msg = "   " + record.msg.replace("\n", "\n   ")
            formatter = logging.Formatter(self.snakemake)
        else:
            formatter = logging.Formatter(self.default)
        return formatter.format(record)


class TracebackInfoFilter(logging.Filter):
    """Clear or restore the exception on log records"""

    def __init__(self, clear=True):
        self.clear = clear

    def filter(self, record):
        if self.clear:
            record._exc_info_hidden, record.exc_info = record.exc_info, None
            # clear the exception traceback text cache, if created.
            record.exc_text = None
        elif hasattr(record, "_exc_info_hidden"):
            record.exc_info = record._exc_info_hidden
            del record._exc_info_hidden
        return True


class SnakemakeFilter(logging.Filter):
    def filter(self, record):
        if record.name.startswith("snakemake."):
            return False
        else:
            return True


class VerboseFilter(logging.Filter):
    def filter(self, record):
        if record.name in VERBOSE_LOGGERS:
            return True
        else:
            return False


def add_logfile(logfile, logger, append=False):
    # Add stream handler
    file = logging.FileHandler(logfile, mode="a" if append else "w")
    file.setFormatter(FileFormatter())
    file.addFilter(TracebackInfoFilter(clear=False))
    logger.addHandler(file)


def set_verbose():
    stream.removeFilter(smfilter)
    stream.addFilter(VerboseFilter())
    stream.setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)
    logger.debug("Setting logging level to verbose...")


# Console formatter
smfilter = SnakemakeFilter()
stream = logging.StreamHandler()
stream.setLevel(logging.INFO)
stream.setFormatter(ConsolFormatter())
stream.addFilter(TracebackInfoFilter(clear=True))
stream.addFilter(smfilter)
logger.addHandler(stream)
