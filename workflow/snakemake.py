import sys

from workflow.logging import logger
from snakemake import snakemake


def run_snakemake(args):
    try:
        logger.info("Running analysis...")
        res = snakemake(
            args["snakefile"],
            configfiles=[args["configfile"]],
            cores=args["processes"],
            workdir=args["work_path"],
            conda_frontend="mamba",
            verbose=False, #args['verbose'],
            quiet=True,
            latency_wait=120,
            use_conda=True,
            printreason=True,
            force_incomplete=True,
            keep_metadata=False,
            lock=False,
            keep_logger=True
        )
        if not res:
            logger.error(f"Something went wrong, please see {args['logfile']}")
            sys.exit(1)
        else:
            logger.info("All done!")
    except Exception as e:
        logger.error(f"Something went wrong, please see {args['logfile']}")
        raise e
