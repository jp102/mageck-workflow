from workflow.config import get_args
from workflow.logging import logger
from workflow.snakemake import run_snakemake


def main():
    args = get_args()
    run_snakemake(args)
