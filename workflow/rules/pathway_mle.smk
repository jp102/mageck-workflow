def get_coefs(mle_design):
    coefs = open(mle_design).readline().split()[2:]
    return coefs

coefs = get_coefs(mle_design)

pathway_mle_target = path.join(mledir, "{coef}-{order}-{pathway}.pathway_summary.txt")

rule pathway_mle:
    input:
        gmt_file = lambda wc: pathways[wc.pathway],
        ranking = path.join(mledir, project + ".gene_summary.txt"),
    output:
        pathway_mle_target
    log:
        path.join(mledir, "logs", "{coef}-{order}-{pathway}.log")
    conda:
        path.join(env_path, "mageck.yaml")
    params:
        column = lambda wc: f"{wc.coef}|beta",
        order = lambda wc: "--reverse_value" if wc.order == "pos" else ""
    wildcard_constraints:
        coef = "|".join(coefs),
        pathway = "|".join(pathways),
        order = "|".join(["pos", "neg"])
    shell:
        "(set -x; mageckGSEA "
        "--rank_file {input.ranking} "
        "--gmt_file {input.gmt_file} "
        "--output_file {output} "
        "--score_column {params.column} " 
        "{params.order}) &> {log}"

rule run_pathway_mle:
    input: expand(pathway_mle_target, coef=coefs, pathway=pathways, order=["pos", "neg"])

