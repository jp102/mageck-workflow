from collections import OrderedDict

# Format for counter config is tsv without a header:
# fastq, label, label, replicate (always '1'), barcode (optional), empty, constant sequence
def write_count_config(filename):
    if 'barcode' in files.columns:
        header = ['path', 'lab1', 'lab2', 'rep', 'barcode', 'empty', 'constant']
        has_barcode = True
    else:
        header = ['path', 'lab1', 'lab2', 'rep', 'prefix_length']
        has_barcode = False

    ret = OrderedDict([(v, list()) for v in header])
    for sample in samples:
        ret['path'].append(path.join(fastqdir, f"{sample}.fastq.gz"))
        ret['lab1'].append(sample)
        ret['lab2'].append(sample)
        ret['rep'].append("1")

        if has_barcode:
            ret['barcode'].append(files.loc[sample].barcode[0])
            ret['empty'].append("")
            ret['constant'].append("")
        else:
            ret['prefix_length'].append(files.loc[sample].prefix_length)

    pd.DataFrame(ret).to_csv(filename, sep='\t', header=False, index=False)

rule counts_config:
    input:
        expand(path.join(fastqdir, "{sample}.fastq.gz"), sample=samples)
    output:
        path.join(countdir, "config.tsv")
    log:
        path.join(countdir, "logs", "config.log")
    run:
        write_count_config(output[0])

rule counts:
    input:
        targets = targets,
        config = path.join(countdir, "config.tsv"),
        files = expand(path.join(fastqdir, "{sample}.fastq.gz"), sample=samples)
    output:
        path.join(countdir, f"{project}_count.txt")
    log:
        path.join(countdir, "logs", "counts.log")
    conda:
        path.join(env_path, "counter.yaml")
    params:
        prefix = path.join(countdir, project),
        barcode = "--NoPrefix" if "barcode" not in files.columns else "",
        mismatch = "-m" if mismatch else "",
        counter = counter
    threads: processes
    shell:
        "(set -x; "
        "java -jar {params.counter} "
        "-i {input.config} -p {input.targets} "
        "-o {params.prefix} "
        "{params.barcode} "
        "{params.mismatch} "
        "-c) &> {log}"

rule run_counts:
    input: path.join(countdir, f"{project}_count.txt")
