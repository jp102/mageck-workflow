rule fastq_combine:
    input:
        get_fastqfiles
    output:
        path.join(fastqdir, "{sample}.fastq.gz")
    log:
        path.join(fastqdir, "logs", "{sample}.log")
    run:
        if len(input) > 1:
            cmd = "cat {input} > {output}"
        else:
            input = os.path.abspath(input[0])
            cmd = "ln -vs {input} {output}"
        shell("(set -x; " + cmd + ") &> {log}")

rule run_fastq_combine:
  input:
      expand(path.join(fastqcdir, "/{sample}.fastq.gz"), sample=samples)

