flute_rra_target = path.join(rradir, "{analysis}", "MAGeCKFlute_{analysis}/FluteRRA_{analysis}.pdf")

rule flute_rra:
    input:
        gene = path.join(rradir, "{analysis}", "{analysis}.gene_summary.txt"),
        sgrna = path.join(rradir, "{analysis}", "{analysis}.sgrna_summary.txt")
    output:
        flute_rra_target
    log:
        path.join(rradir, "logs", "flute-{analysis}.log")
    conda:
        path.join(env_path, "flute.yaml")
    params:
        prefix = path.join(rradir, "{analysis}"),
        project = "{analysis}",
        species = species,
        flute_script = "bin/flute.R"
    shell:
        '(set -x; '
        'export input_genes="{input.gene}"; '
        'export input_sgrna="{input.sgrna}"; '
        'export output_prefix="{params.prefix}"; '
        'export project="{params.project}"; '
        'export species="{params.species}"; '
        'export method="rra"; '
        'R -e \'source("{params.flute_script}")\') &> {log}'

rule run_flute_rra:
    input: expand(flute_rra_target, analysis=rra_samples)

