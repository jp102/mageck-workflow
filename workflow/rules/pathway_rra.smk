pathway_rra_targets = expand(
    path.join(rradir, "{analysis}", "{analysis}-{pathway}.pathway_summary.txt"),
    analysis=analyses,
    pathway=pathways)

rule pathway_rra:
    input:
        gmt_file = lambda wc: pathways[wc.pathway],
        ranking = path.join(rradir, "{analysis}", "{analysis}.gene_summary.txt"),
    output:
        pathway_rra_target = path.join(rradir, "{analysis}", "{analysis}-{pathway}.pathway_summary.txt")
    log:
        path.join(rradir, "logs", "rra-{pathway}-{analysis}.log")
    conda:
        path.join(env_path, "mageck.yaml")
    params:
        prefix = path.join(rradir, "{analysis}", "{analysis}-{pathway}")
        #pathway_options   = f"{pathway_options}" if pathway_options else ""
    shell:
        "(set -x; mageck pathway "
        "-n {params.prefix} "
        "--gene-ranking {input.ranking} "
        "--gmt-file {input.gmt_file}) &> {log}"

rule run_pathway_rra:
    input: pathway_rra_targets

