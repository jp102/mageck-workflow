rule mle:
    input:
        counts = path.join(countdir, f"{project}_count.txt"),
        design = mle_design
    output:
        path.join(mledir, f"{project}.gene_summary.txt"),
    log:
        path.join(mledir, "logs", "mle.log")
    conda:
        path.join(env_path, "mageck.yaml")
    params:
        prefix        = path.join(mledir, project),
        control_sgrna = f"--control-sgrna {control_sgrna}" if control_sgrna else "",
        control_gene  = f"--control-gene {control_gene}" if control_sgrna else "",
        cell_line     = f"--cell-line {cell_line}" if cell_line else "",
        cnv_norm      = f"--cnv-est {cnv_norm}" if cnv_norm else "",
        mle_options   = f"{mle_options}" if rra_options else ""
    shell:
        "(set -x; mageck mle "
        "--count-table {input.counts} "
        "-d {input.design} "
        "-n {params.prefix} "
        "{params.mle_options} "
        "{params.control_sgrna} "
        "{params.control_gene} "
        "{params.cell_line} "
        "{params.cnv_norm}) &> {log}"

rule run_mle:
    input: rules.mle.output

