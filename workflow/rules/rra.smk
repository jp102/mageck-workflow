def get_rra_samples(files, analyses):
    ret = dict()
    for key, val in analyses.items():
        dat = {
            'treatment': ",".join(
                files.loc[files.condition == val['treatment']].index),
            'control': ",".join(
                files.loc[files.condition == val['control']].index)}
        ret[key] = dat
    return ret

rra_samples = get_rra_samples(files, analyses)
rra_targets = expand(path.join(rradir, "{analysis}", "{analysis}.sgrna_summary.txt"), analysis=rra_samples)

rule rra:
    input:
        path.join(countdir, f"{project}_count.txt")
    output:
        path.join(rradir, "{analysis}", "{analysis}.gene_summary.txt"),
        path.join(rradir, "{analysis}", "{analysis}.sgrna_summary.txt")
    log:
        path.join(rradir, "logs", "rra-{analysis}.log")
    conda:
        path.join(env_path, "mageck.yaml")
    params:
        treat = lambda wc: rra_samples[wc.analysis]['treatment'],
        contr = lambda wc: rra_samples[wc.analysis]['control'],
        prefix        = path.join(rradir, "{analysis}", "{analysis}"),
        control_sgrna = f"--control-sgrna {control_sgrna}" if control_sgrna else "",
        control_gene  = f"--control-gene {control_gene}" if control_sgrna else "",
        cell_line     = f"--cell-line {cell_line}" if cell_line else "",
        cnv_norm      = f"--cnv-est {cnv_norm}" if cnv_norm else "",
        rra_options   = f"{rra_options}" if rra_options else ""
    wildcard_constraints:
        analysis = "|".join(analyses),
    shell:
        "(set -x; mageck test "
        "-n {params.prefix} "
        "--count-table {input} "
        "--treatment {params.treat} "
        "--control-id {params.contr} "
        "{params.rra_options} "
        "{params.control_sgrna} "
        "{params.control_gene} "
        "{params.cell_line} "
        "{params.cnv_norm}) &> {log}"

rule run_rra:
    input: rra_targets

