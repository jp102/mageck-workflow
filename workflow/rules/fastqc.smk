rule fastqc:
    input:
        path.join(fastqdir, "{sample}.fastq.gz")
    output:
        path.join(fastqcdir, "{sample}_fastqc.html"),
        path.join(fastqcdir, "{sample}_fastqc.zip"),
    log:
        path.join(fastqcdir, "logs", "{sample}.log")
    params:
        prefix = fastqcdir
    conda:
        path.join(env_path, "fastqc.yaml")
    threads: 2
    shell:
        "(set -x; fastqc -t {threads} --outdir {params.prefix} {input}) &> {log}"

rule fastqc_move:
    input:
        path.join(fastqcdir, "{sample}_fastqc.zip")
    output:
        path.join(fastqcdir, "data", "{sample}_fastqc.zip")
    shell:
        "mv {input} {output}"

rule run_fastqc:
    input:
        expand(path.join(fastqcdir, "data", "{sample}_fastqc.zip"), sample=samples)
