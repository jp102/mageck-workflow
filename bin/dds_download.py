#!/bin/env python

import argparse
import DukeDS

def main(args):
    for remote, local in zip(args.remote, args.local):
        print(f"Downloading {remote} to {local}...")
        DukeDS.download_file(args.project, remote, local)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download a file from DDS.')
    parser.add_argument('--project', help='DDS project')
    parser.add_argument('--remote', nargs="+", help='Remote file')
    parser.add_argument('--local', nargs="+", help='Local file')
    args = parser.parse_args()
    main(args)


