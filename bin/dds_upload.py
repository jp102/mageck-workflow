#!/bin/env python

import DukeDS
import argparse

def main(args):
    for remote, local in zip(args.remote, args.local):
        print(f"Uploading {local} to {remote}...")
        DukeDS.upload_file(args.project, local, remote)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Upload a file to DDS.')
    parser.add_argument('--project', help='DDS project')
    parser.add_argument('--remote', nargs="+", help='Remote file')
    parser.add_argument('--local', nargs="+", help='Local file')
    args = parser.parse_args()
    main(args)


