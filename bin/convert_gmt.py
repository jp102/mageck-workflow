#!/bin/env python
import argparse

from collections import defaultdict

"""\
Convert MiSig GMT file using orthologues in given chip file and list of valid gene symbols
"""

# chip and gmt files are tab-delimited
SEP = "\t"

# in gmt file, pathways start on column 2
GMT_COL = 2


def read_chip(args):
    ret = defaultdict(set)
    with open(args.chip, "r") as infh:
        for line in infh:
            dat = line.split(SEP)
            ret[dat[1]].update([dat[0]])
    return ret


def write_converted(args, chip):
    outfile = open(args.outfile, "w")
    gmtfile = open(args.gmt, "r")
    for line in gmtfile:
        dat = line.strip().split(SEP)
        genes = set()
        for v in dat[3:-1]:
            try:
                genes.update(chip[v])
            except KeyError:
                pass
        ret = dat[0:2] + sorted(genes)
        print("\t".join(ret), file=outfile)


def main(args):
    chip = read_chip(args)
    write_converted(args, chip)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--chip", required=True, help="MiSig chip file")
    parser.add_argument("--gmt", required=True, help="MiSig gmt file")
    parser.add_argument("--outfile", required=True, help="output file")
    args = parser.parse_args()
    main(args)
