# Duke Functional Genomics Core MAGeCK workflow

Automating the counting and analysis of sgRNA libraries using [MAGeCK](https://sourceforge.net/p/mageck/wiki/Home/).

## Running
The workflow can be run using the `mageck-workflow` command.

A simple RRA analysis can be run by running:
```
./mageck-workflow --project <project-name> --samples <samplefile> --targets <targetfile>
```

A typical run, will result in the following output:
```
<project-name>
  |--fastqc/
     |--<sample>.html
  |--counts/
     |--<project>_count.txt
     |--<project>_NucleotideFrequency.pdf
  |--mageck-rra/
     |--<comparison>/
        |--<comparison>.gene_summary.txt
        |--<comparison>.sgrna_summary.txt
  |--<project>.log
  |--samples.csv
  |--config.yaml
```

# Example
Checkout an example workflow [here](docs/example-rra.md)

## Input files
### Samples
A table of samples is required for the analysis. This the location of this file 
should be passed to the option `--samples` and have the following format:
| sample | condition | prefix_length | path                |
|--------|-----------|---------------|---------------------|
| C1     | control   | 10            | path/to/C1.fastq.gz |
| C2     | control   | 11            | path/to/C2.fastq.gz |
| T1     | treatment | 12            | path/to/T1.fastq.gz |
| T2     | treatment | 13            | path/to/T2.fastq.gz |

### Targets
Target files are stored in the `resources/libraries` directory and can be used to supply the `--targets` option.
For example:
```
./mageck-workflow <options...> --targets resources/libraries/Brunello_fix.csv
```

### MLE
MLE analyses require a design matrix a file, see the [MAGeCK documentation for more information](https://sourceforge.net/p/mageck/wiki/input/#design-matrix-file)

### CNV Correction
MAGeCK can try to account for known differences in copy-number. This requires an external resource available via the [DepMap project](https://depmap.org/portal/download)
Under files, download `CCLE_gene_cn.csv` [direct download](https://ndownloader.figshare.com/files/34989937).


## Options
```
usage: ./mageck-workflow <options> ...
  Required:
    --project           <string>           project name and location to output results
    --samples           <sample file>      file describing samples and conditions
    --targets           <target file>      file describing CRISPR targets and their sequences
  Options:
    --treatment         [<condition> ...]  treatment conditions (one or more) to use in RRA analysis
    --control           [<condition> ...]  control conditions (one or more) to use for in RRA analysis
    --mle_design        <design file>      design matrix for MLE analysis (off by default, required for MLE)
    --species           <species>          species to use for gene-set enrichment analysis (default: human)
    --cnv_norm          <cnv file>         copy number variation data across cell lines to normalize against
    --control_sgrna     <sgrna file>       list of control sgRNAs to normalize against
    --control_genes     <gene file>        list of control genes to normalize against
    --cell_line         <cell-line>        name of the cell line to be used for copy number normalization
    --config            <config>           analysis configuration file
    --constant_sequence <sequence>         sgRNA constant sequence (default: 'TCTTGTGGAAAGGACGAAACACCG')
    --input_prefix      <path>             location to look for samples (default is current directory)
    --output_prefix     <path>             location to save results (default is current directory)
    --processes         <number>           number of concurrent tasks to run (default is half the number of CPUs)
    --counter           <program>          path to Java based barcode-counter
    --rra_options       <options>          additional options to pass to mageck-rra
    --no_rra                               do not run MAGeCK RRA 
    --no_pathway                           do not run gene-set enrichment workflow
    --no_flute                             do not run MAGeCKFlute on any comparisons
    --no_counts                            do not tabulate sgRNA counts
    --mismatch                             allow a single mismatch between the read and sgRNA sequence
    --verbose                              more detailed logging
    --continue                             add to / continue existing analysis
    --force                                remove existing project folder and existing data
    -h, --help                             show help message and exit
```

## Installation
Once this repository is cloned, initial setup is automated using the `setup.sh` script. 
```
git clone git@gitlab.oit.duke.edu:jp102/mageck-workflow.git
cd mageck-workflow
./setup.sh
```

Once complete, the command `mageck-workflow` can be used run the analysis.  
Please note that addition software will be downloaded during the first run of the workflow.

