# Multiple condition RRA Analysis
We will be testing the effect of Drug in either WT or Mu conditions. Using RRA, we can answer the following questions:

1. What is the effect of Drug in WT
2. What is the effect of Drug in the Mu
3. Absent Drug, is there a difference between WT and Mu 

For this analysis we will assume we have the following conditions in triplicate:

| condtion           | short-hand |
|--------------------|-----------|
| Time Zero          | Early     |
| WT exposed to Drug | WT-Drug   |
| WT without Drug    | WT-NoDrug |
| Mu exposed to Drug | Mu-Drug   |
| Mu without Drug    | Mu-NoDrug |

And we will be interested in the following comparisons:
 * WT-Drug vs. WT-NoDrug
 * Mu-Drug vs. Mu-NoDrug
 * WT-Drug vs. Mu-Drug
 * WT-NoDrug vs. Mu-NoDrug


## Project setup
Let's also assume that everything for this project is located in a folder named `sgrna-analyses`, and within this folder
is the workflow cloned from GitLab, as well as a folder called `projects/`, such that:

```
sgrna-analyses/                    <- assume this has the path ~/Desktop
  |-- mageck-workflow/              <- this is the workflow directory cloned from GitLab containing the mageck_workflow script
     |-- projects/                  <- a place for projects 
        |-- example_project/        <- the workspace for this project
          |-- data/                 <- location of the data for this project
            |-- <fastq files>       
          |-- config/               <- all project configuration files
            |-- samples.tsv        
            |-- config.yaml
          |-- <analysis output>/    <- the intended output path for this analysis (created by the workflow)
```

Once the workflow has been cloned and setup, you will want to create a samples.tsv file.

## Define samples
`samples.tsv` contains the name of the sample, condition it is associated with, barcode
information and the location of the fastq file.


For this project, the file would resemble the following:

| sample       | condition | prefix_length | path                                                             |
|--------------|-----------|---------------|------------------------------------------------------------------|
| Early-01     | WT-Early  | 1             | ~/Desktop/sgrna-analyses/example_project/data/early-01.fa.gz     |
| WT-Drug-01   | WT-Drug   | 2             | ~/Desktop/sgrna-analyses/example_project/data/wt-drug--01.fa.gz  |
| WT-NoDrug-01 | WT-NoDrug | 3             | ~/Desktop/sgrna-analyses/example_project/data/wt-nodrug-01.fa.gz |
| Mu-Drug-01   | Mu-Drug   | 4             | ~/Desktop/sgrna-analyses/example_project/data/mu-drug-01.fa.gz   |
| Mu-NoDrug-01 | Mu-NoDrug | 5             | ~/desktop/sgrna-analyses/example_project/data/mu-nodrug-01.fa.gz |
| ...etc...    | ...       | ...           | ...                                                              |
| Mu-NoDrug-03 | Mu-NoDrug | 15            | ~/desktop/sgrna-analyses/example_project/data/mu-nodrug-03.fa.gz |

## Command line arguments
By default, the workflow will produce RRA results for each pairwise comparison. 
We can then invoke the following command, and if all goes well the workflow will produce
all the comparisons we are interested in:

From the `mageck-workflow/` folder, we can run:
```
magic_workflow \ 
  --project example_analysis \
  --targets resources/libraries/Brunello_fix.csv \
  --samples projects/example_project/samples.tsv \
  --output_prefix projects/example_project
```

This is telling the workflow that, using the samples and conditions provided in `samples.tsv`,
to run RRA on each pairwise set of samples, using the `Brunello_fix.csv` library of sgRNAs
and output the results to our `example_project/` directory within `projects/`


## Specific Comparisons
There are two ways to control which samples are going to be paired in the analyses:

### Setting control and treatment samples explicitly:
Using the `--control` and `--treatment` options, one can control which samples are going to be compared.
Each option can take at least one condition (as defined in `samples.csv`), for example:

```
magic_workflow \ 
  --project example_analysis \
  --targets resources/libraries/Brunello_fix.csv \
  --samples projects/example_project/samples.tsv \
  --control WT-Early \
  --treatment WT-NoDrug WT-Drug \
  --output_prefix projects/example_project
```

Will produce analyses:
 * WT-NoDrug vs. WT-Early
 * WT-Drug vs. WT-Early


### Letting the workflow decide
The workflow will, by default run all pairwise comparisons, and it will determine
the order using the order of the conditions in `samples.csv`. The first condition 
listed is used as the first control, followed by the second, and so forth. 
For example, the conditions in `samples.csv` are : `A`, `B`, `C`, and `D` (in this order),
the workflow would run the following analysis:

| treatment | control |
|-----------|---------|
| B         | A       |
| C         | A       |
| D         | A       |
| C         | B       |
| D         | B       |
| D         | C       |

For this reason, it is recommended to list the samples representing the most relevant controls
first when preparing `samples.csv`. Using the example above, we may want to re-order these to:

| sample       | condition | prefix_length | path                                                             |
|--------------|-----------|---------------|------------------------------------------------------------------|
| Early-01     | WT-Early  | 1             | ~/Desktop/sgrna-analyses/example_project/data/early-01.fa.gz     |
| WT-NoDrug-01 | WT-NoDrug | 3             | ~/Desktop/sgrna-analyses/example_project/data/wt-nodrug-01.fa.gz |
| Mu-NoDrug-01 | Mu-NoDrug | 5             | ~/desktop/sgrna-analyses/example_project/data/mu-nodrug-01.fa.gz |
| WT-Drug-01   | WT-Drug   | 2             | ~/Desktop/sgrna-analyses/example_project/data/wt-drug--01.fa.gz  |
| Mu-Drug-01   | Mu-Drug   | 4             | ~/Desktop/sgrna-analyses/example_project/data/mu-drug-01.fa.gz   |
| ...etc...    | ...       | ...           | ...                                                              |
